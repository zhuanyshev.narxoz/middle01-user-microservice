FROM openjdk:17-oracle
MAINTAINER ilyas
COPY build/libs/middle01userservice-0.0.1-SNAPSHOT.jar userservice.jar
ENTRYPOINT ["java", "-jar", "userservice.jar"]
