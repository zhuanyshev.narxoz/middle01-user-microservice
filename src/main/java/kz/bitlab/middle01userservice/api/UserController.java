package kz.bitlab.middle01userservice.api;


import kz.bitlab.middle01userservice.model.User;
import kz.bitlab.middle01userservice.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/user")
@RequiredArgsConstructor
public class UserController {

    private final UserRepository userRepository;

    private final String URL_CONFIG = "http://some-url";

    private final String URL_SETUP = "http://setup-url-test-config-token";

    @GetMapping
    public List<User> getUsers(){
        return userRepository.findAll();
    }

    @GetMapping(value = "{id}")
    public User getUser(@PathVariable(name = "id") Long id){
        return userRepository.findById(id).orElse(null);
    }

    private String generateToken(){
        return "some token";
    }
}